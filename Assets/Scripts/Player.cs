﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	public float forcaPulo;
	public float velocidadeMaxima;

	public int lives;
	public int rings;

	public Text TextLives;
	public Text TextRings;

	public bool isGrounded;

	public bool canFly;
	
    void Start ()
	{
		//TextLives.text = lives.ToString();
		//TextRings.text = rings.ToString();
	}

    void Update()
    {
     //Player Movimentation
       // Allow the movimentation W/S in the player
       Rigidbody2D rigidbody = GetComponent<Rigidbody2D>();
       float movimento = Input.GetAxis("Horizontal");
       rigidbody.velocity = new Vector2(movimento*velocidadeMaxima,rigidbody.velocity.y);
       //Apply the flip Player
 		if (movimento < 0)
		{
			GetComponent<SpriteRenderer>().flipX = true;
		}else if (movimento > 0)
		{
			GetComponent<SpriteRenderer>().flipX = false;
		}
       //Movimentation animation
		if (movimento > 0 || movimento < 0)
		{
			GetComponent<Animator>().SetBool("Walking", true);
		}
		else
		{
			GetComponent<Animator>().SetBool("Walking", false);
		}

		if (Input.GetKeyDown(KeyCode.Space) )
		{
			if (isGrounded)
			{
				rigidbody.AddForce(new Vector2(0, forcaPulo));
				GetComponent<AudioSource>().Play();
				canFly = false;
			}
			else
			{
				canFly = true;
			}
		}
    
       //Fly
		if (canFly && Input.GetKey(KeyCode.Space))
		{
			GetComponent<Animator>().SetBool("Flying", true);
			rigidbody.velocity = new Vector2(rigidbody.velocity.x,-0.5f);
		}
		else
		{
			GetComponent<Animator>().SetBool("Flying", false);
		}
		
		if (isGrounded)
		{
			GetComponent<Animator>().SetBool("Jumping", false);
		}
		else
		{
			GetComponent<Animator>().SetBool("Jumping", true);
		}
		
	}
   
   
    void OnCollisionEnter2D(Collision2D collision2D)
    {
        if( collision2D.gameObject.CompareTag("Monstros"))
        {
            //xD
        }
        if( collision2D.gameObject.CompareTag("Plataformas"))
        {
            isGrounded = true;
        }
        if( collision2D.gameObject.CompareTag("Moedas"))
        {
            //xD
        }
    }
	void OnCollisionExit2D(Collision2D collision2D)
	{
		if ( collision2D.gameObject.CompareTag("Plataformas"))
		{
			isGrounded = false;
		}
	}
}


